package com.endava.hr.repository.impl;

import com.endava.hr.entity.Department;
import com.endava.hr.entity.Employee;
import com.endava.hr.exception.ResourceNotFoundException;
import com.endava.hr.repository.DepartmentRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Optional;

import static java.util.Objects.isNull;

@Repository
public class DepartmentRepositoryImpl implements DepartmentRepository {

    public static final String SELECT_FROM_DEPARTMENTS = "from Department";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public Collection<Department> getAll(Boolean extend) {
        return entityManager.createQuery(SELECT_FROM_DEPARTMENTS).getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Department> get(Long id, Boolean extend) {
        Department department = entityManager.find(Department.class, id);
        return Optional.ofNullable(department);
    }

    @Override
    public void create(Department department)  {
        entityManager.persist(department);
    }

    @Override
    public void update(Department department) {
       Department persistedDepartment = entityManager.find(Department.class, department.getDepartmentId());

        if (isNull(persistedDepartment)) {
            throw new ResourceNotFoundException("Department to update not found.");
        }

        Employee persistedEmployee = entityManager.find(Employee.class, department.getManager().getEmployeeId());

        if (isNull(persistedEmployee)) {
            throw new ResourceNotFoundException("Manager with such ID for this department not found.");
        }

        persistedDepartment.setDepartmentName(department.getDepartmentName());
        persistedDepartment.setManager(department.getManager());
        persistedDepartment.setLocationId(department.getLocationId());
    }

    @Override
    public void deleteById(Long id) {
        Department persistedDepartment = entityManager.find(Department.class, id);

        if(isNull(persistedDepartment)) {
            throw new ResourceNotFoundException("Department with such ID not found.");
        }

        entityManager.remove(persistedDepartment);
    }

}

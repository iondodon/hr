package com.endava.hr.repository.impl;

import com.endava.hr.entity.Employee;
import com.endava.hr.exception.ResourceNotFoundException;
import com.endava.hr.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.Optional;

import static java.util.Objects.isNull;

@Repository
@RequiredArgsConstructor
public class EmployeeRepositoryImpl implements EmployeeRepository {
    public static final String SELECT_FROM_EMPLOYEES = "from Employee";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public Collection<Employee> getAll(Boolean extend) {
        return entityManager.createQuery(SELECT_FROM_EMPLOYEES).getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Employee> get(Long id, Boolean extend) {
        Employee employee = entityManager.find(Employee.class, id);
        return Optional.ofNullable(employee);
    }

    @Override
    @Transactional
    public void create(Employee employee) {
        entityManager.persist(employee);
    }


    @Override
    @Transactional
    public void update(Employee employee) {
        Employee persistedEmployee = entityManager.find(Employee.class, employee.getEmployeeId());

        if (isNull(persistedEmployee)) {
            throw new ResourceNotFoundException("Employee with id = " + employee.getEmployeeId() + " not found.");
        }

        persistedEmployee.setFirstName(employee.getFirstName());
        persistedEmployee.setLastName(employee.getLastName());
        persistedEmployee.setEmail(employee.getEmail());
        persistedEmployee.setPhoneNumber(employee.getPhoneNumber());
        persistedEmployee.setHireDate(employee.getHireDate());
        persistedEmployee.setJobId(employee.getJobId());
        persistedEmployee.setSalary(employee.getSalary());
        persistedEmployee.setCommissionPct(employee.getCommissionPct());
        persistedEmployee.setDepartmentId(employee.getDepartmentId());
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        Employee employee = entityManager.find(Employee.class, id);

        if (isNull(employee)) {
            throw new ResourceNotFoundException("Employee not found.");
        }

        entityManager.remove(employee);
    }

}
